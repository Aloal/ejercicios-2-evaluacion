package unidad5;


public class Autor {

	  private String nombre;
	  private String email;
	  private String genero;
	  
	  
	  public Autor(String nombre, String email, String genero) {
			this.nombre=nombre;
			this.email=email;
			this.genero=genero;
		}


	public String getNombre() {
		return nombre;
	}


	public String getEmail() {
		return email;
	}


	public String getGenero() {
		return genero;
	}


	
	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public String toString() {
		return "Autor [nombre=" + nombre +   ", genero=" + genero +  ", email=" + email + ", ]";
	}

  
public static void main(String[] args) {
		
		//probamos el metodo toString
		
		Autor primero=new Autor("Valle Inclan","valle@homtail.com","(maculino)");
		System.out.println(primero.toString());
		
		//probamos los metodos get
		
		 Autor segundo=new Autor("Cervantes","cerva@homtail.com","(maculino)");
		 
		 System.out.println("Autor segundo=" + segundo.getNombre()+ " " + segundo.getGenero()+" "+ segundo.getEmail());
		 
          
		 //probabmos el metodo set y get del email
		 
		 segundo.setEmail("cervantes@hotmail.com");
		 System.out.printf("Nuevo email de Cervantes=" + segundo.getEmail());
		 
	}
	  
	  
}
