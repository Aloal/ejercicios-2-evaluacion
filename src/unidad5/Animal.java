package unidad5;

import java.time.LocalDate;

public class Animal {
	
	private String nombre;
	private LocalDate fecha;
	
	public Animal(String nombre, LocalDate fecha) {
		this.nombre=nombre;
		this.fecha=fecha;
	}
	
	public Animal(String nombre) {
		this.nombre=nombre;
		fecha=LocalDate.now();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	@Override
	public String toString() {
		return "Animal [nombre=" + nombre + ", fecha=" + fecha + "]";
	}

	
	public static void main(String[] args) {
		Animal perro =new Animal("Peter");
        System.out.println(perro.toString());
        
         // Creamos un nuevo objeto
        
        Animal gato=new Animal("Boni");
        
        // cambiamos nombre del objeto gato
        
        gato.setNombre("lucy");
        System.out.println("gato nombre=" + gato.getNombre());
        
        
        
        
	}
	
}
