package unidad5;


   enum Autores{ CERVANTES, HEMINWAY,CELA};

public class Libro {
	
	private String titulo;
	public Autores autor;                         
	private float precio;
	private int stock;
	
    
	public Libro() {
		
	}
	
	public Libro(String titulo, Autores autor,float precio) {
		this.titulo=titulo;
		this.autor=autor;
		this.precio=precio;
		this.stock=0;
	}
	
	public Libro(String titulo, Autores autor,float precio,int stock) {
		this.titulo=titulo;
		this.autor=autor;
		this.precio=precio;
		this.stock=stock;
	}

	public String getTitulo() {
		return titulo;
	}

	public Autores getAutor() {
		return autor;
	}

	public float getPrecio() {
		return precio;
	}

	public int getStock() {
		return stock;
	}

	public void setPrecio(float precio) {
		this.precio = precio;
	}

	public void setStock(int stock) {
		this.stock = stock;
	}

	@Override
	public String toString() {
		return "Libro [titulo=" + titulo + ", autor=" + autor + ", precio=" + precio + ", stock=" + stock + "]";
	}
	


	
   public static void main(String[] args) {
	
	
	 Autores autor1;
	 Autores autor2;              // declaramos vables del tipo enum.
	 Autores autor3;  
	
    autor1=Autores.CERVANTES;         // asignamos valores a la vbles declaradas, un valor de los enum.
    autor2=Autores.CELA;
    autor3=Autores.HEMINWAY;
    
  //probamos el metodo toString
	
  		Libro primerlibro=new Libro();
  		Libro segundolibro=new Libro("El quijote", autor1,15,6);
  		Libro tercerLibro=new Libro("La comunidad",autor2,12,5);
  		
  		System.out.println(tercerLibro.toString());
  	
  		System.out.println("Trecer libro= "  + tercerLibro.getTitulo()+ ", " +"autores:" +"("+ tercerLibro.getAutor()+" y "+ autor1 +")"+" " + tercerLibro.getPrecio()+ "�" + ", " +tercerLibro.getStock()+ "unidades en stock");
	


      }
}