package unidad7;

public class Lavadora extends Electrodomesticos {
	
	  
	private int cargaLavadora=5;
		

	public int getCargaLavadora() {
		return cargaLavadora;
	}

		//Constructores
	
	public Lavadora(double precio, double peso) {
		super(precio, peso);
		this.cargaLavadora=cargaLavadora;
	}

	public Lavadora(double precio, String color, char consumo, double peso) {
		super(precio, color, consumo, peso);
		this.cargaLavadora=cargaLavadora;
	}
	
	  
    
	public double precioFinal() {
	    double cargo =super.precioFinal();
		 
	    if(cargaLavadora>8) {
	    	cargo=cargo+(precio*10)/100;
	    }
	    return cargo;
	}

	
	public void mostrar() {
	
		
	}

	
	
	
}
