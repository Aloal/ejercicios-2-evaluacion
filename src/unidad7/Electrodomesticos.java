package unidad7;


public abstract class Electrodomesticos {

     
	  protected double precio=100;
	  protected String color="blanco";
	  protected char consumo='F';
	  protected double peso=5;
	   
	  
	  	    
	  public Electrodomesticos(double precio, double peso) {
		  this.precio=precio;
		  this.peso=peso;
	  }
	  	 
		   
	  	 
	public Electrodomesticos(double precio, String color, char consumo, double peso) {
		
		this.precio = precio;
		this.color = color;
		this.consumo = consumo;
		this.peso = peso;
	}

     
	 	
	 

	public double getPrecio() {
		return precio;
	}



	public String getColor() {
		return color;
	}



	public char getConsumo() {
		return consumo;
	}



	public double getPeso() {
		return peso;
	}


	  

	@Override
	public String toString() {
		return "Electrodomesticos [precio=" + precio + ", color=" + color + ", consumo=" + consumo + ", peso=" + peso
				+ "]";
	}


	private void comprobarColor(String color){
		   
        //Colores disponibles
		
        String colores[]={"blanco", "negro", "rojo", "azul", "gris"};
        boolean encontrado=false;
  
        for(int i=0;i<colores.length && !encontrado;i++){
              
            if(colores[i].equals(color)){
                encontrado=true;
            }
              
        }
         
        
        if(encontrado){
            this.color=color;
        }else{
            this.color="blanco";
        }
          
          
    }
	
	
	      	
	

	public double precioFinal() {
		double cargo=0;
		switch(consumo) {
		
		case 'A':
			cargo+=(precio*30)/100;
		  break;
		  
		case 'B':
			cargo+=(precio*25)/100;
		  break; 
		  
		case 'C':
			cargo+=(precio*20)/100;
		  break;
		  
		case 'D':
			cargo+=(precio*15)/100;
		  break;  
		  
		case 'E':
			cargo+=(precio*10)/100;
		  break;
		  
		case 'F':
			cargo+=(precio*5)/100;
		  break;   
		}
		  
		
		if (peso > 0 && peso <= 19) {
            cargo +=(precio*5)/100;
            
        } else if (peso >= 20 && peso <= 49) {
            cargo += (precio*10)/100;
            
        } else if (peso >= 50 && peso <= 79) {
        	  cargo += (precio*15)/100;
            
        }else if (peso > 80) {
        	 cargo += (precio*20)/100;
        	 
        }  
		  
		  
		 return precio+cargo; 
		  
		 		
		}
	
	public abstract void mostrar();
	}


	